package bo.com.fie.cognos.rest.restpersona.entidades;

import java.io.IOException;
import java.util.Base64;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

// Por esta etiqueta el servicio lo llama, asi actua como un interceptor.
@Provider
public class PresonaRestFilter implements ContainerRequestFilter {

	public void filter(ContainerRequestContext requestContext) throws IOException {
		// TODO Auto-generated method stub
		String autorizacion = requestContext.getHeaderString("Authorization");
		String[] datosAut = autorizacion.split(" ");
		byte[] arreglo = Base64.getDecoder().decode(datosAut[1]);
		String datoDecodificado = new String(arreglo);
		String metodo = requestContext.getMethod();
		String uri = requestContext.getUriInfo().getPath();
		System.out.println("autorizacion: " + autorizacion);
		System.out.println("autorizacion desifrada: " + datoDecodificado);
		System.out.println("metodo: " + metodo);
		System.out.println("uri: " + uri);

		// Se verifican las credenciales para evitar que siga el ciclo.
		String[] datosCredencial = datoDecodificado.split(":");
		String usuario = datosCredencial[0];
		String password = datosCredencial[1];
		if (usuario.equals("jjvos") && password.equals("contrasenha")) {
			System.out.println("Credenciales correctas");
		} else {
			System.err.println("Usuario no autenticado correctamente");
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}

}
