package bo.com.fie.cognos.rest.restpersona.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

// Permite definir el path de inicio de la aplicacion.
@ApplicationPath("rest")
public class AppConfig extends Application {

}
