package bo.com.fie.cognos.rest.restpersona.db;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.session.SqlSession;

import bo.com.fie.cognos.rest.restpersona.entidades.Persona;

public class PersonaDAO {
	public List<Persona> listarPersonas() {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		List<Persona> personas = mapper.listPersona();
		session.close();
		return personas;
	}

	public Persona getPersona(long id) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		Persona p = mapper.getPersona(id);
		session.close();
		return p;
	}

	public boolean insertarPersona(Persona persona) {
		boolean bandera;
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		try {
			mapper.insertarPersona(persona);
			session.commit();
			bandera = true;
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
			bandera = false;
		}
		session.close();
		return bandera;
	}

	public Persona actualizarPersona(Persona persona) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		Persona personaActualizada = null;
		try {
			int afectados = mapper.actualizarPersona(persona);
			System.out.println("Afectados: " + afectados);
			personaActualizada = afectados == 1 ? persona : null;
			session.commit();
		} catch (Exception ex) {
			System.err.println("Error al actualizar: " + ex.getMessage());
		}
		session.close();
		return personaActualizada;
	}

	public boolean borrarPersona(long id) {
		boolean bandera;
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		try {
			bandera = mapper.borrarPersona(id);
			session.commit();
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
			bandera = false;
		}
		session.close();
		return bandera;
	}

}
