package bo.com.fie.cognos.rest.restpersona.entidades;

import java.io.Serializable;

public class Persona implements Serializable {
	/*
	 CREATE TABLE public.persona
(
    id integer NOT NULL DEFAULT nextval('persona_id_seq'::regclass),
    nombre character varying(50) COLLATE pg_catalog."default",
    paterno character varying(50) COLLATE pg_catalog."default",
    materno character varying(50) COLLATE pg_catalog."default",
    edad numeric,
    CONSTRAINT persona_pkey PRIMARY KEY (id)
) 
	 **/
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Persona() {
		this.id = 0;
		this.nombre = "?";
		this.paterno = "?";
		this.materno = "?";
		this.edad = -1;
	}

	public Persona(long id, String nombre, String paterno, String materno, int edad) {
		this();
		this.id = id;
		this.nombre = nombre;
		this.paterno = paterno;
		this.materno = materno;
		this.edad = edad;
	}

	private long id;
	private String nombre;
	private String paterno;
	private String materno;
	private int edad;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

}
