package bo.com.fie.cognos.rest.restpersona.services;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import bo.com.fie.cognos.rest.restpersona.db.PersonaDAO;
import bo.com.fie.cognos.rest.restpersona.entidades.Persona;
import bo.com.fie.cognos.rest.restpersona.entidades.RespuestaServicio;

@Path("personas")
public class PersonaRest {
	
	
	private PersonaDAO personaDao = new PersonaDAO();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPersonas() {
		Response response = null;
		List<Persona> lista = personaDao.listarPersonas();
		System.out.println("Ingresando a lista");
		if (lista != null && !lista.isEmpty()) {
		 
		response = (lista != null && !lista.isEmpty()) ? Response.ok().entity(lista).build()
				: Response.status(Response.Status.NOT_FOUND).build();
		} else {
			response = Response.ok().entity(new RespuestaServicio("001", "No existen datos a mostrar")).build();
		}
		return response;
	}

	// @PathParam Se utiliza cuando se pasa los parametros por la URL.
	@GET
	@Path("/buscar/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersona(@PathParam("id") long id) {
		Persona p = personaDao.getPersona(id);
		Response response = p != null ? Response.ok().entity(p).build()
				: Response.status(Response.Status.NOT_FOUND).build();
		return response;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insertarPersona(Persona persona) {
		boolean bandera = personaDao.insertarPersona(persona);
		Response response = bandera ? Response.ok().build() : Response.status(Response.Status.NOT_ACCEPTABLE).build();
		return response;
	}
	
	// En la url ha que adicionar http://localhost:8080/rest-personas/rest/personas/form
	// POST y en body se le coloca x-www-form-urlencode
	// @FormParam Se utiliza para indicar que pasa por formulario los datos.
	@POST
	@Path("/form")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertarPersona(@FormParam("nombre") String nombre, 
			@FormParam("paterno") String paterno, 
			@FormParam("materno") String materno, 
			@FormParam("edad") int edad) {
		Persona persona = new Persona();
		persona.setEdad(edad);
		persona.setMaterno(materno);
		persona.setNombre(nombre);
		persona.setPaterno(paterno);
		boolean bandera = personaDao.insertarPersona(persona);
		Response response = bandera ? Response.ok().build() : Response.status(Response.Status.NOT_ACCEPTABLE).build();
		return response;
	}
	

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizarPersona(Persona persona) {
		Persona personaActualizada = personaDao.actualizarPersona(persona);
		Response response = personaActualizada != null ? Response.ok().entity(personaActualizada).build()
				: Response.status(Response.Status.NOT_ACCEPTABLE).build();
		return response;
	}

	@DELETE
	@Path("/{id}")
	public Response borrarPersona(@PathParam("id") long id) {
		boolean bandera = personaDao.borrarPersona(id);
		Response response = bandera ? Response.ok().build() : Response.status(Response.Status.NOT_ACCEPTABLE).build();
		return response;
	}

}
