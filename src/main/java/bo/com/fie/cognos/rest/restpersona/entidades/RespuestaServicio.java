package bo.com.fie.cognos.rest.restpersona.entidades;

public class RespuestaServicio {

	private String codigo;
	private String mensaje;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public RespuestaServicio() {
		this.codigo = "?";
		this.mensaje = "?";
	}
	
	public RespuestaServicio(String codigo, String mensaje) {
		this();
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
}
