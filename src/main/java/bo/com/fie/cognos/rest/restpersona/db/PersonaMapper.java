package bo.com.fie.cognos.rest.restpersona.db;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import bo.com.fie.cognos.rest.restpersona.entidades.Persona;

public interface PersonaMapper {

	// Puede devolver ninguno, uno o varios registros.
	@Select("SELECT * FROM persona")
	List<Persona> listPersona();

	// Puede devolver ninguno, uno o varios registros.
	@Select("Select * from persona where id = #{id}")
	Persona getPersona(long id);

	// Devuelve la cantidad de registros insertados
	@Insert("insert into persona(nombre, paterno, materno, edad) values (#{nombre}, #{paterno}, #{materno}, #{edad})")
	void insertarPersona(Persona persona);
	
	// Devuelve la cantidad de registros afectados
	@Update("update persona set nombre = #{nombre}, paterno = #{paterno}, materno = #{materno}, edad = #{edad} where id = #{id} ")
	int actualizarPersona(Persona persona);
	
	// Devuelve la cantidad de registros afectados
	@Delete("delete from persona where id = #{id}")
	boolean borrarPersona(long id);
}
